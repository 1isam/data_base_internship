# 这是2019数据库课程设计的代码仓库

## # Java Eclipse和JDK的安装以及git的简单使用
- 使用环境windows 7 Ultimate 32位, [eclipse neon 3 win32](http://coding-net-production-file-1257242599.cos.ap-shanghai.myqcloud.com/56da0310-182b-11ea-bc16-a98a3f2fa02e.zip?sign=q-sign-algorithm%3Dsha1%26q-ak%3DAKIDay83lFmaS6Y4LTdzMVO1SdZOyJSNOYpr%26q-sign-time%3D1575707191%3B1575710791%26q-key-time%3D1575707191%3B1575710791%26q-header-list%3D%26q-url-param-list%3Dresponse-content-disposition%3Bresponse-expires%26q-signature%3D05d021e931c50ccca47c076a15f272034076c40c&response-content-disposition=attachment%3Bfilename%3Declipse-java-neon-3-win32.zip&response-expires=Mon%2C%2009%20Dec%202019%2020%3A26%3A31%20GMT), [jdk 8u231 windows i586](http://coding-net-production-file-1257242599.cos.ap-shanghai.myqcloud.com/8f40e7b0-182a-11ea-bc16-a98a3f2fa02e.exe?sign=q-sign-algorithm%3Dsha1%26q-ak%3DAKIDay83lFmaS6Y4LTdzMVO1SdZOyJSNOYpr%26q-sign-time%3D1575707271%3B1575710871%26q-key-time%3D1575707271%3B1575710871%26q-header-list%3D%26q-url-param-list%3Dresponse-content-disposition%3Bresponse-expires%26q-signature%3De26334ea7d64827f7c018523614c5a5df773c702&response-content-disposition=attachment%3Bfilename%3Djdk-8u231-windows-i586.exe&response-expires=Mon%2C%2009%20Dec%202019%2020%3A27%3A51%20GMT), [git 2.24.0.2 32bit](http://coding-net-production-file-1257242599.cos.ap-shanghai.myqcloud.com/9300fbc0-1810-11ea-97ea-d1159ab4f38e.exe?sign=q-sign-algorithm%3Dsha1%26q-ak%3DAKIDay83lFmaS6Y4LTdzMVO1SdZOyJSNOYpr%26q-sign-time%3D1575707335%3B1575710935%26q-key-time%3D1575707335%3B1575710935%26q-header-list%3D%26q-url-param-list%3Dresponse-content-disposition%3Bresponse-expires%26q-signature%3Da8cd98c98a97b119dc50999fede2b53d758daddc&response-content-disposition=attachment%3Bfilename%3DGit-2.24.0.2-32-bit.exe&response-expires=Mon%2C%2009%20Dec%202019%2020%3A28%3A55%20GMT), [mysql-connector java 8.0.18](http://coding-net-production-file-1257242599.cos.ap-shanghai.myqcloud.com/e9410530-180f-11ea-97ea-d1159ab4f38e.jar?sign=q-sign-algorithm%3Dsha1%26q-ak%3DAKIDay83lFmaS6Y4LTdzMVO1SdZOyJSNOYpr%26q-sign-time%3D1575707396%3B1575710996%26q-key-time%3D1575707396%3B1575710996%26q-header-list%3D%26q-url-param-list%3Dresponse-content-disposition%3Bresponse-expires%26q-signature%3D0df82f00de850537f357863e0bf72f6471783ea2&response-content-disposition=attachment%3Bfilename%3Dmysql-connector-java-8.0.18.jar&response-expires=Mon%2C%2009%20Dec%202019%2020%3A29%3A56%20GMT)
## Part 1 eclipse和jdk的安装
1. 下载eclipse和jdk, 链接在上面, 这里我给的eclipse是便携版本, 解压缩后找到主程序, 点击运行即可; 右键可以生成快捷方式到桌面; 在没有安装jdk之前如果打开eclipse会是这个样子:

![图片1](https://res.cloudinary.com/loneykids/image/upload/v1575709528/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/db44e877-49b8-473a-a4c4-1fb536163969_lqbxlv.png)

需要安装jdk, 点击jdk安装文件:

![图片2](https://res.cloudinary.com/loneykids/image/upload/v1575709534/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/28dc5110-73f7-4ee7-b079-8dd4da38d0b4_jbzvup.jpg)

点击下一步:

![图片3](https://res.cloudinary.com/loneykids/image/upload/v1575709534/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/8c215a78-dd0f-4ba6-b239-f52c84363fb1_edpxyj.jpg)

点击关闭:

![图片4](https://res.cloudinary.com/loneykids/image/upload/v1575709535/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/93ba36d0-bab9-435e-a4ee-34de923d3acd_xnim5v.jpg)

在cmd中输入java, 出现下面这种表示安装成功:

![图片5](https://res.cloudinary.com/loneykids/image/upload/v1575709528/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/eb926dda-5ab8-474c-b81d-ddd01e5ab02e_y4oggj.jpg)

打开eclipse, 不需要配置环境变量, 点击File, 找到Import:

![图片6](https://res.cloudinary.com/loneykids/image/upload/v1575709529/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/0c41cff3-cd55-4a0c-9657-0243d0be23c4_yeh1gs.jpg)

选择从git中导入项目, 点击下一步:

![图片7](https://res.cloudinary.com/loneykids/image/upload/v1575709529/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/86c53447-64cc-4e84-b607-67912bae63a4_j8eljn.jpg)

选择从url克隆项目:

![图片8](https://res.cloudinary.com/loneykids/image/upload/v1575709529/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/3f1bc3f9-0451-47b8-80d4-23789918e9d4_usmvnt.jpg)

在我们的git项目中找到clone链接, 点击复制:

![图片9](https://res.cloudinary.com/loneykids/image/upload/v1575709527/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/e062f0dc-2bca-4992-9d25-7db5d0b193e4_njoahk.jpg)

粘贴到URL框中, 输入自己的账号密码, 并点击保存密码:

![图片10](https://res.cloudinary.com/loneykids/image/upload/v1575709526/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/af17f7ca-dbd0-4261-a59b-5877e27c0abb_mhm8ll.png)

点击下一步, 选择要克隆到本地的分支:

![图片11](https://res.cloudinary.com/loneykids/image/upload/v1575709533/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/1b92bd58-d95c-4a1a-8cbd-010476ae86c6_nzcyvz.png)

点击下一步, 选择项目在本地的储存位置:

![图片12](https://res.cloudinary.com/loneykids/image/upload/v1575709535/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/8391432c-a06f-451b-8f77-8c40c30d0e5e_aoyylx.png)

点击下一步, 选择默认克隆向导, 作为已存在的eclipse项目导入

![图片13](https://res.cloudinary.com/loneykids/image/upload/v1575709530/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/e43b94a4-2a41-4688-833a-a903327d26a9_vrok9q.jpg)

点击下一步, 最后确认, 点击Finish

![图片14](https://res.cloudinary.com/loneykids/image/upload/v1575709530/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/239c7f56-d6f0-400a-b203-a38398b1f36f_c9vwsk.jpg)

导入完成, 可以在project栏看到导入的项目, 由于每台电脑的jdk目录不一样, 很有可能会报error:

![图片15](https://res.cloudinary.com/loneykids/image/upload/v1575709530/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/933bd8da-e623-445d-89fe-6b4de2a4f5e4_zw2k8x.jpg)

此时在项目上右键, 选择Bulid Path, 点击配置Build Path:

![图片16](https://res.cloudinary.com/loneykids/image/upload/v1575709528/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/ea5acd9d-5baa-4061-af17-42e3eed39b46_coilik.png)

点击Libiaries, 配置外部库, 点击添加外部jars:

![图片17](https://res.cloudinary.com/loneykids/image/upload/v1575709531/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/93427e85-44b3-4832-886d-52559bd551f5_xtzc5m.jpg)

找到提供的mysql连接器:

![图片18](https://res.cloudinary.com/loneykids/image/upload/v1575709531/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/c255be1a-34df-4320-af96-b77cc5d67fce_gbkgv7.png)

复制到图片中的位置, 点击继续:

![图片19](https://res.cloudinary.com/loneykids/image/upload/v1575709531/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/01ef5fec-6e84-47c0-9298-5d80fb5f4f47_jhdjmg.png)

然后回到添加外部jars的步骤, 选择mysql连接器:

![图片20](https://res.cloudinary.com/loneykids/image/upload/v1575709531/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/7cce3da4-f0d0-4a08-a788-cb0223eb3859_vqb6rg.png)

添加完成后, 点击确认:

![图片21](https://res.cloudinary.com/loneykids/image/upload/v1575709531/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/c8eda8e4-e430-4cca-8109-ae4f819fd063_h7fug4.jpg)

然后此时就可以运行Java程序了:

![图片22](https://res.cloudinary.com/loneykids/image/upload/v1575709532/%E5%8D%9A%E5%AE%A2%E5%9B%AD%E5%9B%BE%E5%BA%8A/java%20git%20mysql/597b0ef4-c498-4ac5-b573-fb026f7ee4a5_pkioqh.jpg)

## 此时已经完成所需要的基本配置

###  MySQL数据库放在阿里云
- 使用时首先判断自己的ip是否在数据库允许连接的范围, 点击[ip.cn](https://ip.cn)查看自己的ip地址, 将整个网段都加入白名单即可访问, 由于ip地址经常变动(关机重启等情况最容易使ip变动), 如果发现拒绝访问的情况, 首先排查是否ip地址在白名单

- 使用的是MySQL 5.7
